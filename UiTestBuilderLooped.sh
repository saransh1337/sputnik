fileName=$1
loopFor=$2

for ((i = 1; i <= $loopFor; i++))

do

ant build


adb push bin/MyAutomationTest001.jar /data/local/tmp


adb shell uiautomator runtest MyAutomationTest001.jar -c com.uia.example.sars.$fileName

echo "#-------------------------------------------------------------------------------"
echo "#-----------------The last test script run was successful-----------------------"
echo "#-------------------------------------------------------------------------------"
echo "#---Running test: $fileName ------------------------------------------------"
echo "#---Test script count: $i of $loopFor ------------------------------------------"
echo "#-------------------------------------------------------------------------------"
echo "#-------------------------------------------------------------------------------"

done


 