fileName01=$1
fileName02=$2
loopFor=$3

for ((i = 1; i <= $loopFor; i++))

do

ant build


adb push bin/MyAutomationTest001.jar /data/local/tmp


adb shell uiautomator runtest MyAutomationTest001.jar -c com.uia.example.sars.$fileName01

echo "#------Test $fileName01 done---"

ant build


adb push bin/MyAutomationTest001.jar /data/local/tmp


adb shell uiautomator runtest MyAutomationTest001.jar -c com.uia.example.sars.$fileName02

echo "#-------------------------------------------------------------------------------"
echo "#-----------------The last test script run was successful-----------------------"
echo "#-------------------------------------------------------------------------------"
echo "#---Running test: $fileName01 ------------------------------------------------"
echo "#---Test script count: $i of $loopFor -----------------------------------------------"
echo "#-------------------------------------------------------------------------------"
echo "#-------------------------------------------------------------------------------"
done


 
