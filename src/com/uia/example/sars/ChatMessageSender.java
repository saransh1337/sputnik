package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/14/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ChatMessageSender extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {


        //todo: make this as the nandu chatter test
        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        TalktoTestUtils.searchFor("Dan Dannett");

        TalktoTestUtils.sendMessage("message " +TalktoTestUtils.randomGenerator());

        UiObject shareMenuButton = new UiObject(new UiSelector().resourceId("to.talk:id/content_sharing_button"));
        shareMenuButton.click();



    }
}


