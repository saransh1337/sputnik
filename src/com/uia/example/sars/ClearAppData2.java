package com.uia.example.sars;

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ClearAppData2 extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException, InterruptedException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        TalktoTestUtils.openApplicationUsingADB();

        sleep(5000);

        //open recent apps
        try {
            getUiDevice().pressRecentApps();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        sleep(5000);

        UiObject talktoRecentApp = new UiObject(new UiSelector().resourceId("com.android.systemui:id/app_thumbnail").descriptionContains("Talk.to"));
        talktoRecentApp.longClick();

        UiObject appInfoButton = new UiObject(new UiSelector().resourceId("android:id/title").text("App info"));
        appInfoButton.clickAndWaitForNewWindow();

        UiObject forceStop = new UiObject(new UiSelector().resourceId("com.android.settings:id/left_button"));
        forceStop.click();

        sleep(5000);

        UiObject okButton = new UiObject(new UiSelector().resourceId("android:id/button1"));
        okButton.click();

        sleep(5000);

       if (TalktoTestUtils.isButtonEnabled(forceStop,8)) System.out.println("Button is disabled and stays disabled after 8 seconds");


    }
}