package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/14/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;

import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ManualNotX extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException, InterruptedException {

        /**
         * wake the device up
         * and get to home screen by pressing home
         **/

        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            //
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        /**
         * check if single step sign in is available
         * if not,
         * report 'NO Single step sign in available
         **/

        if (TalktoTestUtils.is0StepSigninAvailable()){

            System.out.println("0 step sign in is available.Tapping 'skip' for this test.");
            TalktoTestUtils.skipButtonTapper();

        }
        else {
            System.out.println("NO 0 step sign in available");
        }

        /**
         * sign up for talk.to using passed first name,
         * last name and phone number
         **/

        TalktoTestUtils.talktoSignUp("Sam", "Harris", "9717166637");

        /**
         * on the 'Welcome back!' screen,
         * tap 'Not X' button
         * a message box will pop up asking if X is really your number
         * tap 'yes'
         **/

        TalktoTestUtils.tapNotXConfirm("Yes");

        /**
         * on the verification screen,
         * if auto verification doesn't happen in 10 seconds,
         * wait for another 10
         **/

        TalktoTestUtils.isAutoVerificationDone();

        sleep(10000);

        //tap done post auto verification
        TalktoTestUtils.tapDoneOnProfileUpload();

        TalktoTestUtils.swipeSkipFeatureTour();

        //check if app exists in fg, 10 sec after sign up

        sleep(10000);

        TalktoTestUtils.checkAppHomeScreenExistsInFg(5);



    }
}
