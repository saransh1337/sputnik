package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/12/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ManualSignIn extends UiAutomatorTestCase {

  public void  testDemo() throws UiObjectNotFoundException, RemoteException, InterruptedException {

      //marker13
      String phoneNumber = "9717166637";
      String password = "freetop";

      getUiDevice().wakeUp();

      getUiDevice().pressHome();

      try {
          getUiDevice().setOrientationNatural();
      } catch (RemoteException e) {
          e.printStackTrace();
      }

      TalktoTestUtils.openApplicationUsingADB();

      TalktoTestUtils.skipAutoSignUp();

      sleep(3000);

      //String phoneNumber = "9717166637";

      TalktoTestUtils.talktoSignIn(phoneNumber,password); //Todo: ideally, pass phone number and the password as arguments to this test

      sleep(3000);

      TalktoTestUtils.swipeSkipFeatureTour();

      sleep(10000);

      TalktoTestUtils.checkAppHomeScreenExistsInFg(5);
  }



}

