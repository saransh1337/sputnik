package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/12/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ManualSignUp extends UiAutomatorTestCase {

  public void testDemo() throws UiObjectNotFoundException, RemoteException {

      getUiDevice().wakeUp();

      getUiDevice().pressHome();

      try {
          getUiDevice().setOrientationNatural();
      } catch (RemoteException e) {
          e.printStackTrace();
      }

      TalktoTestUtils.openApplicationUsingADB();

      try {
          if (TalktoTestUtils.is0StepSigninAvailable()){

              TalktoTestUtils.skipButtonTapper();

          }
      } catch (UiObjectNotFoundException e) {

          e.printStackTrace();
      }

      TalktoTestUtils.talktoSignUp("Sam", "Harris", "9717166637");

      //todo check if now the script is at welcome back screen. if yes. delete this meta by calling a method that does that and resume sign up by back trackig in the flow a bit

      //TalktoTestUtils.waitForAutoVerification();

      if(TalktoTestUtils.isAutoVerificationDone()) {

          System.out.println("Auto-verification Done.");

      }
      else {

          System.out.println("Auto-verification failed.");

      }

      sleep(5000);

      TalktoTestUtils.tapDoneOnProfileUpload();

      TalktoTestUtils.swipeSkipFeatureTour();


      sleep(10000);

      TalktoTestUtils.checkAppHomeScreenExistsInFg(5);

  }

}