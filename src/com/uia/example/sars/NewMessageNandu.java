package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/18/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class NewMessageNandu extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        TalktoTestUtils.newMessageNandu("9871971695","hello world 1234");

        getUiDevice().pressBack();
    }

        public void newMessageTango(String contactNameNumber, String msgPayload) throws UiObjectNotFoundException {

            //tap on new message button
            UiObject newMessageButton = new UiObject(new UiSelector().resourceId("to.talk:id/menu_home_activity_start_conversation"));
            newMessageButton.clickAndWaitForNewWindow();

            //search for contact in string contactName
            UiObject contactSearch = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));
            contactSearch.setText(contactNameNumber);

            //enter the message
            UiObject messageBox = new UiObject(new UiSelector().resourceId("to.talk:id/message_field"));
            messageBox.setText(msgPayload);

            UiObject sendButton = new UiObject(new UiSelector().resourceId("to.talk:id/send"));

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("2 sec thread sleep in newMessageNandu method failed");
            }

            sendButton.click();




    }

    }
