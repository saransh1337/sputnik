package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/22/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class RosterScrollStressTester extends UiAutomatorTestCase {

    int topScrollCounter = 0;
    int bottomScrollCounter = 0;

    public void testDemo() throws UiObjectNotFoundException {

        /**
         * wake the device up
         * and get to home screen by pressing home
         **/

        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            //
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        sleep(4000);

        TalktoTestUtils.goToTab("CONTACTS");

        rosterScrollToBottom();

        sleep(20000);

        rosterScrollToTop();

    }

    public void rosterScrollToBottom() throws UiObjectNotFoundException {

        // the page is declared scollable here
        UiScrollable scrollableRoster = new UiScrollable(new UiSelector().resourceId("to.talk:id/viewpager").scrollable(true));

        // initial marker contact is picked up pre scroll
        UiObject contactMarker1 = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        // from contact marker, a unique phone number is picked
        UiObject contactUniqueNumber1 = contactMarker1.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder1 = contactUniqueNumber1.getText();
        System.out.println("**The first unique number is:" +uniqueHolder1);

        sleep(2000);

        // 1 flick downward
        scrollableRoster.flingForward();

        sleep(2000);

        //the secondary contact marker is picked up
        UiObject contactMarker2 = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        // from secondary contact marker, a unique phone number is picked
        UiObject contactUniqueNumber2 = contactMarker2.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder2 = contactUniqueNumber2.getText();
        System.out.println("--The second unique number is:" +uniqueHolder2 +"\n");

        //int scrollCounter = 0;

        if (uniqueHolder1 != uniqueHolder2) {

            bottomScrollCounter++;
            rosterScrollToBottom();
        }
        else  {
            System.out.println("Reached the end of the roster counter in "+bottomScrollCounter +" swipes.");
        }

    }


    public void rosterScrollToTop() throws UiObjectNotFoundException {

        // the page is declared scollable here
        UiScrollable scrollableRoster = new UiScrollable(new UiSelector().resourceId("to.talk:id/viewpager").scrollable(true));

        // initial marker contact is picked up pre scroll
        UiObject contactMarker1 = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        // from contact marker, a unique phone number is picked
        UiObject contactUniqueNumber1 = contactMarker1.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder1 = contactUniqueNumber1.getText();
        System.out.println("**The first unique number is:" +uniqueHolder1);

        sleep(2000);

        // 1 flick upward
        scrollableRoster.flingBackward();

        sleep(2000);

        //the secondary contact marker is picked up
        UiObject contactMarker2 = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        // from secondary contact marker, a unique phone number is picked
        UiObject contactUniqueNumber2 = contactMarker2.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder2 = contactUniqueNumber2.getText();
        System.out.println("--The second unique number is:" +uniqueHolder2 +"\n");

        //int scrollCounter = 0;

        if (uniqueHolder1 != uniqueHolder2) {

            topScrollCounter++;
            rosterScrollToTop();

        }
        else  {
            System.out.println("Reached the end of the roster counter in "+topScrollCounter +" swipes.");
        }

    }

}





/*
    public void rosterScrollToBottom() throws UiObjectNotFoundException {

        UiScrollable scrollableRoster = new UiScrollable(new UiSelector().resourceId("to.talk:id/viewpager").scrollable(true));
        scrollableRoster.flingForward();
        UiObject contactMarker = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        UiObject contactUniqueNumber = contactMarker.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder = contactUniqueNumber.getText();
        System.out.println("checkpoint1");
        if (uniqueHolder.equals(contactUniqueNumber.getText())) {
            rosterScrollToBottom();
        }
        else {
            System.out.println("Reached the end of the roster");
        }

    }
*/

// scrollable roster object

//UiObject scrollableRoster = new UiObject(new UiSelector().resourceId("to.talk:id/viewpager"));

//UiScrollable scrollableRoster = new UiScrollable(new UiSelector().resourceId("to.talk:id/viewpager").scrollable(true));
//scrollableRoster.scrollToEnd(7);
//scrollableRoster.getMaxSearchSwipes();
//scrollableRoster.scrollToEnd(scrollableRoster.getMaxSearchSwipes());

//sleep(5000);

//scrollableRoster.scrollToBeginning(scrollableRoster.getMaxSearchSwipes());
/*
        int scrollCounter = scrollableRoster.getMaxSearchSwipes();
        while (scrollCounter > 0) {

            scrollableRoster.flingForward();
            scrollCounter--;
            sleep(10000);
        }
*/
//scrollableRoster.flingForward();

//System.out.println(scrollableRoster.getMaxSearchSwipes());
//System.out.println("checkpoint1");

/*
        UiObject contactMarker = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
        UiObject contactUniqueNumber = contactMarker.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
        String uniqueHolder = contactUniqueNumber.getText();
        while (uniqueHolder.equals(contactUniqueNumber.getText())) {
            scrollableRoster.flingForward();
            System.out.println("checkpoint1");
        }
*/



/*
        do  {
            scrollableRoster.flingForward();
            UiObject contactMarker = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(2));
            UiObject contactUniqueNumber = contactMarker.getChild(new UiSelector().resourceId("to.talk:id/subtext"));
            String uniqueHolder = contactUniqueNumber.getText();
            System.out.println("checkpoint1");
        }
        while (uniqueHolder.equals(contactUniqueNumber.getText()));
*/