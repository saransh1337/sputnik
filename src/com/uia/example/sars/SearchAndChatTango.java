package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/23/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import android.widget.LinearLayout;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

/**
 * Created by saranshsharma on 2/17/14.
 */
public class SearchAndChatTango extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        sleep(6000);

        UiObject searchButton = new UiObject(new UiSelector().resourceId("to.talk:id/menu_home_activity_search_contact"));
        searchButton.clickAndWaitForNewWindow();

        String lookUpName = "Sarah";

        UiObject searchBox = new UiObject(new UiSelector().resourceId("to.talk:id/search_box"));
        searchBox.setText(lookUpName);

        sleep(4000);
/*
        UiScrollable searchResults = new UiScrollable(new UiSelector().className(android.widget.ListView.class.getName()).scrollable(true));
        UiObject contactFinder = searchResults.getChildByText(new UiSelector().className(android.widget.LinearLayout.class.getName()),lookUpName);


        contactFinder.clickAndWaitForNewWindow();
*/

        //Getting the scrollable view

        UiScrollable settingsList = new UiScrollable(new UiSelector().scrollable(true));

        for (int i=0;i<=settingsList.getChildCount(new UiSelector ().className(LinearLayout.class.getName()));i++){
        //Looping through each linear layout view
            UiObject linearLayout = settingsList.getChild(new UiSelector().className(LinearLayout.class.getName()).instance(i));

        //Checking if linear layout have the text. If yes, get the switch, click and break out of the loop.
            if(linearLayout.getChild(new UiSelector ().text(lookUpName)).exists()){
                UiObject contactTapable = linearLayout.getChild(new UiSelector().className(android.widget.Switch.class.getName()));
                contactTapable.click ();
                break;
            }
        }

        UiObject result = new UiObject(new UiSelector().text(lookUpName));



    }
}
