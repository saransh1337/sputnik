package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/16/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class SetDefaultFromBlurb extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        sleep(7000);

        UiObject defaultPromptTitle = new UiObject(new UiSelector().className("android.widget.TextView").text("Do more with SMS on Talk.to"));

        if (defaultPromptTitle.getText().equals("Do more with SMS on Talk.to")) {

            System.out.println("Default SMS prompt is present.");

            TalktoTestUtils.setDefaultFromBlurb();

            sleep(3000);

        }

        sleep(10000);

        TalktoTestUtils.checkAppHomeScreenExistsInFg(2);

    }
}

