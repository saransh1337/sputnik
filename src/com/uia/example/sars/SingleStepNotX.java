package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/18/14.
 */

import android.os.RemoteException;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;



public class SingleStepNotX extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        sleep(5000);

        TalktoTestUtils.is0StepSigninAvailable();

        TalktoTestUtils.singleStepSignUp();

        sleep(24000);

        TalktoTestUtils.tapNotX();

        sleep(20000);

        TalktoTestUtils.tapDoneOnProfileUpload();

        sleep(7000);

        TalktoTestUtils.swipeSkipFeatureTour();


        sleep(10000);

        TalktoTestUtils.checkAppHomeScreenExistsInFg(1);

    }
}
