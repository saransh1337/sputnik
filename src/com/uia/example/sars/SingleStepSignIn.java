package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/17/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class SingleStepSignIn extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {

        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        try {
            getUiDevice().setOrientationNatural();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        TalktoTestUtils.openApplicationUsingADB();

        sleep(5000);

        TalktoTestUtils.is0StepSigninAvailable();

        TalktoTestUtils.singleStepSignUp(); //todo when signin takes too long the test fails. make it more robust

        sleep(24000);

        TalktoTestUtils.tapContinueAsX();

        sleep(5000);

        TalktoTestUtils.swipeSkipFeatureTour();


        sleep(10000);

        TalktoTestUtils.checkAppHomeScreenExistsInFg(1);


    }
}
