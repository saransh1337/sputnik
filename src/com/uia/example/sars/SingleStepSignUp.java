package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/17/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries

import android.os.RemoteException;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class SingleStepSignUp extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException, RemoteException {

        getUiDevice().wakeUp();

        getUiDevice().pressHome();

        TalktoTestUtils.openApplicationUsingADB();

        sleep(5000);

        TalktoTestUtils.is0StepSigninAvailable();

        TalktoTestUtils.singleStepSignUp();

        sleep(30000);

        TalktoTestUtils.swipeSkipFeatureTour();


        sleep(10000);

        TalktoTestUtils.checkAppHomeScreenExistsInFg(1); //test

    }

}

