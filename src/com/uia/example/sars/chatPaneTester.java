package com.uia.example.sars;

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;
import android.util.Log;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class ChatPaneTester extends UiAutomatorTestCase {

    public void testDemo() throws UiObjectNotFoundException {


        //todo: make this as the nandu chatter test
        //wake the device up
        try {
            getUiDevice().wakeUp();
        } catch (RemoteException e) {
            System.out.println("Device wake up failed");
            e.printStackTrace();
        }

        getUiDevice().pressHome();

        TalktoTestUtils.openApplicationUsingADB();

        TalktoTestUtils.searchFor("Dan Dannett");

        TalktoTestUtils.sendMessage("message " +TalktoTestUtils.randomGenerator());

    }
}


