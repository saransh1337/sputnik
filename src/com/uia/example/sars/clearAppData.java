package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/10/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class ClearAppData extends UiAutomatorTestCase {

public void testDemo() throws UiObjectNotFoundException, InterruptedException {

    //wake the device up
	try {
		getUiDevice().wakeUp();
	} catch (RemoteException e) {
		System.out.println("Device wake up failed");
		e.printStackTrace();
	}

	getUiDevice().pressHome();

    try {
        getUiDevice().setOrientationNatural();
    } catch (RemoteException e) {
        e.printStackTrace();
    }

    TalktoTestUtils.openApplicationUsingADB();

    sleep(5000);

    ttAppInfoFromRecentApps();

    UiObject clearDataButton = new UiObject(new UiSelector().resourceId("com.android.settings:id/right_button").text("Clear data"));

    if(TalktoTestUtils.isButtonEnabled(clearDataButton, 15)) {

        clearDataButton.click();
    }
    else System.out.println("Test failed as the 'Clear Data' button was disabled");


    sleep(5000);

    UiObject alertTitle = new UiObject(new UiSelector().resourceId("android:id/alertTitle"));
    assert (alertTitle.exists()) : "Couldn't find Clear Data conformation message box";

    UiObject okClearData = new UiObject((new UiSelector().resourceId("android:id/button1")));
    okClearData.click();

    sleep(3000);

    getUiDevice().pressHome();

    }

    public void ttAppInfoFromRecentApps() throws UiObjectNotFoundException {

        try {
            getUiDevice().pressRecentApps();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        sleep(5000);


        UiScrollable recentScroller = new UiScrollable(new UiSelector().resourceId("com.android.systemui:id/recents_container"));
        if (recentScroller.isScrollable()) {
            System.out.println("recentScroller is scrollable");
            UiObject ttFromRecent = recentScroller.getChildByDescription(new UiSelector().resourceId("com.android.systemui:id/app_thumbnail"), "Talk.to");
            ttFromRecent.longClick();
        } else if (!recentScroller.isScrollable()) {
//            System.out.println("The recent apps list is empty. phone must have been recently restarted or this list was cleared. recentScroller " +
//                    "isnt scrollable");
            UiObject ttFromRecent = recentScroller.getChildByDescription(new UiSelector().resourceId("com.android.systemui:id/app_thumbnail"), "Talk.to");
            ttFromRecent.longClick();

        }

        UiObject appInfoButton = new UiObject(new UiSelector().resourceId("android:id/title").text("App info"));
        appInfoButton.clickAndWaitForNewWindow();


    }


/*
    public void getTalktoFromRecentAppsOld() throws UiObjectNotFoundException { // deprecated. delete later.

        try {
            getUiDevice().pressRecentApps();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        sleep(5000);

        //getTalktoFromRecentApps();

        UiObject talktoRecentApp = new UiObject(new UiSelector().resourceId("com.android.systemui:id/app_thumbnail").descriptionContains("Talk.to"));
        talktoRecentApp.longClick();

        UiObject appInfoButton = new UiObject(new UiSelector().resourceId("android:id/title").text("App info"));
        appInfoButton.clickAndWaitForNewWindow();
    }
*/

}





