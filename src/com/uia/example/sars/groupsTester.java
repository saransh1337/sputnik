package com.uia.example.sars;

/**
 * Created by saranshsharma on 2/20/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.os.RemoteException;
import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.uia.helper.sars.TalktoTestUtils;

public class GroupsTester extends UiAutomatorTestCase {

public void testDemo() throws UiObjectNotFoundException, RemoteException {
 
	//test002q

    getUiDevice().wakeUp();

    getUiDevice().pressHome();

	TalktoTestUtils.openApplicationUsingADB();


	//go to Chats tab
	TalktoTestUtils.goToTab("CHATS");
    TalktoTestUtils.goToTab("GROUPS");

    createGroup("ScriptedGrp001","Stephan");

    sleep(3000);

    addToGroup("Dan Dannett");


/*
    //tap on create new group button
    UiObject createGroupButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group"));
    createGroupButton.clickAndWaitForNewWindow();

    //check if you are on create new group page
    UiObject createGroupTitle =  new UiObject(new UiSelector().resourceId("android:id/action_bar_title"));
    assertTrue("Test failed to reach the Create group page",createGroupTitle.getText().equals("Create a new group"));
    System.out.println("Script has successfully reached the create new group page");

    //name the group
    UiObject nameNewGroup = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_name"));
    nameNewGroup.setText("Scripted Grp0001"); //todo pass the group name as a para meter to a group creator method

    //tap next
    UiObject createGrpNext = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_proceed_item"));
    createGrpNext.clickAndWaitForNewWindow();

    UiObject addPersonField = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));

    addPersonField.setText("Stephan");
    sleep(500);

    UiDevice uiDevice = getUiDevice();
    uiDevice.click(addPersonField.getBounds().centerX(), addPersonField.getBounds().bottom + 20);
    sleep(500);

    UiObject addPersonDoneButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_done_item"));
    addPersonDoneButton.clickAndWaitForNewWindow();

    addToGroup("Dan Dannett");

    sleep(4000);

    getUiDevice().pressBack();

    TalktoTestUtils.sendMessage("qwerty");
*/


}

    public void createGroup(String groupName, String personNameNumber) throws UiObjectNotFoundException {

        //Todo find a way to use this and get rid of above code

        TalktoTestUtils.goToTab("GROUPS");


        //tap on create new group button
        UiObject createGroupButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group"));
        createGroupButton.clickAndWaitForNewWindow();

        //check if you are on create new group page
        UiObject createGroupTitle =  new UiObject(new UiSelector().resourceId("android:id/action_bar_title"));
        assertTrue("Test failed to reach the Create group page",createGroupTitle.getText().equals("Create a new group"));
        System.out.println("Script has successfully reached the create new group page");

        //name the group
        UiObject nameNewGroup = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_name"));
        nameNewGroup.setText(groupName);

        //tap next
        UiObject createGrpNext = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_proceed_item"));
        createGrpNext.clickAndWaitForNewWindow();

        UiObject addPersonField = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));

        addPersonField.setText(personNameNumber);
        sleep(500);

        UiDevice uiDeviceInstance = getUiDevice();
        uiDeviceInstance.click(addPersonField.getBounds().centerX(), addPersonField.getBounds().bottom + 20);
        sleep(500);

        UiObject addPersonDoneButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_done_item"));
        addPersonDoneButton.clickAndWaitForNewWindow();

        sleep(8000);

        if (createGroupTitle.exists()) {
            System.out.println("Test failed at 'add people to group' page. Disconnection or possible crash");
        }

        //getUiDevice().pressBack();


    }

    public void addToGroup(String personNameNumber) throws UiObjectNotFoundException {

        //Todo: this should ideally take groupName as a parameter as well, then find the group and add person

        UiObject groupInfoButton = new UiObject(new UiSelector().resourceId("to.talk:id/chat_pane_menu_group_info"));
        groupInfoButton.clickAndWaitForNewWindow();

        UiObject addPeopleButton = new UiObject(new UiSelector().resourceId("to.talk:id/group_info_add_people"));
        addPeopleButton.clickAndWaitForNewWindow();

        UiObject addPersonField = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));

        addPersonField.setText(personNameNumber);
        sleep(500);

        UiDevice uiDeviceInstance = getUiDevice();
        uiDeviceInstance.click(addPersonField.getBounds().centerX(), addPersonField.getBounds().bottom + 20);
        sleep(500);

        UiObject addPersonDoneButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_done_item"));
        addPersonDoneButton.clickAndWaitForNewWindow();

        //UiObject backfromGroupInfo = new UiObject(new UiSelector().className("android.widget.LinearLayout").descriptionContains("Add people, Navigate up"));
        //backfromGroupInfo.clickAndWaitForNewWindow();
    }






/*
	//go to Groups tab
	UiObject groupsTab = new UiObject(new UiSelector().resourceId("to.talk:id/title").text("GROUPS"));
	groupsTab.click();
	
	//tap on create new group button
	UiObject createGroupButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group"));
	createGroupButton.clickAndWaitForNewWindow();
	
	//check if you are on create new group page
	UiObject createGroupTitle =  new UiObject(new UiSelector().resourceId("android:id/action_bar_title"));
	
	assertTrue("Test failed to reach the Create group page",createGroupTitle.getText().equals("Create a new group"));
    System.out.println("Script has successfully reached the create new group page");
*/

	
	
//	UiObject newGrpNameField = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_name"));
//	newGrpNameField.
	
	
}


	