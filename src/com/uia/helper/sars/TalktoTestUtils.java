package com.uia.helper.sars;

/**
 * Created by saranshsharma on 2/12/14.
 */

//import junit.framework.TestCase;

//Import the uiautomator libraries
import android.util.Log;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

import java.io.*;

public class TalktoTestUtils extends UiAutomatorTestCase {

    public static void openApplicationUsingADB() {
        String queryString = "am start -n to.talk/to.talk.device.ui.activities.Startup";
        try {
            Runtime.getRuntime().exec(queryString);
            Thread.sleep(2000);
        } catch (IOException e) {
            e.printStackTrace();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //check if 0 step sign in is offered. If yes, we then have 'skip' and '0 step sign in' options on that screen
    public static boolean is0StepSigninAvailable() throws UiObjectNotFoundException {
        // TODO fill this up
        try{
            UiObject _0stepSigninButton = new UiObject(new UiSelector().resourceId("to.talk:id/zero_sign_up_button"));

            if (_0stepSigninButton.getText().equals("Sign in to Talk.to")){
                return true;
            }
            else {
                return false;
            }
        }
        catch (UiObjectNotFoundException ex) {
            Log.v("0 Sign in button" ,"UI object not found exception: Could not find 0 step sign in button");
            return false;
        }

    }

    public static void skipAutoSignUp() throws UiObjectNotFoundException {

        try {
            if (TalktoTestUtils.is0StepSigninAvailable()){

                TalktoTestUtils.skipButtonTapper();

            }
        } catch (UiObjectNotFoundException e) {

            //e.printStackTrace();
            System.out.println("test failed to skip auto signup"); //ToDO change this text later. less ambiguity
        }

    }

    public static void singleStepSignUp() throws UiObjectNotFoundException {

        UiObject singleStepTapper = new UiObject(new UiSelector().resourceId("to.talk:id/zero_sign_up_button"));

        try {
            if (TalktoTestUtils.is0StepSigninAvailable()){

                singleStepTapper.clickAndWaitForNewWindow();

            }
        } catch (UiObjectNotFoundException e) {

            e.printStackTrace();
        }

    }


    //sign in

    public static void talktoSignUp(String userFirstName, String userLastName, String userPhoneNumber) throws UiObjectNotFoundException {
        //TODO fill this up
        try{

            UiSelector firstNameSelector = new UiSelector();
            UiObject firstName =new UiObject((firstNameSelector.resourceId("to.talk:id/sign_up_first_name")));
            firstName.clearTextField();
            firstName.setText(userFirstName);

            //assertTrue("Unable to change First Name",  firstName.getText().equals("Sam"));


            UiSelector lastNameSelector = new UiSelector();
            UiObject lastName =new UiObject((lastNameSelector.resourceId("to.talk:id/sign_up_last_name")));
            lastName.clearTextField();
            lastName.setText(userLastName);

            //assertTrue("Unable to change Last Name",  lastName.getText().equals("Harris"));

            UiSelector phNumberSelector = new UiSelector();
            UiObject phNumber =new UiObject((phNumberSelector.resourceId("to.talk:id/sign_up_number_field")));
            phNumber.setText(userPhoneNumber);


            UiObject nextSignUpButton =new UiObject((phNumberSelector.resourceId("to.talk:id/sign_up_next_button")));
            nextSignUpButton.clickAndWaitForNewWindow();

        }
        catch (UiObjectNotFoundException ex) {
            Log.v("Sign up/Sign in" ,"UI object not found exception: Sign up/Sign in user on first screen");
        }
    }

    public static void skipButtonTapper() throws UiObjectNotFoundException {
        UiObject skipButton = new UiObject(new UiSelector().resourceId("to.talk:id/normal_sign_up_button"));
        skipButton.clickAndWaitForNewWindow();

    }

    public static void _0stepButtonTapper() throws UiObjectNotFoundException {
        UiObject _0stepButton = new UiObject(new UiSelector().resourceId("to.talk:id/zero_sign_up_button"));
        _0stepButton.clickAndWaitForNewWindow();
    }

    // tap Yes or No to confirm phone number during the 'Not X' flow
    public static void tapNotXConfirm(String YesOrNo) throws UiObjectNotFoundException {
        // tap on 'not X'
        UiObject notX =new UiObject((new UiSelector().resourceId("to.talk:id/sign_up_welcome_back_not_you")));
        notX.clickAndWaitForNewWindow();

        UiObject myNumberYes =new UiObject((new UiSelector().resourceId("to.talk:id/not_you_yes_option")));
        UiObject myNumberNo =new UiObject((new UiSelector().resourceId("to.talk:id/not_you_no_option")));

        if (YesOrNo.equals(myNumberYes.getText())) {

            myNumberYes.clickAndWaitForNewWindow();

        }
        else {
            if (YesOrNo.equals(myNumberNo.getText())) {

                myNumberNo.clickAndWaitForNewWindow();

            }
        }

        //todo: handle case where yes or no buttons dont work for some reason or yes/no wasnt tapped successfully

    }

    public static boolean isAutoVerificationDone() throws UiObjectNotFoundException {

        //wait for auto verification to be done successfully

        try {
            Thread.sleep(20000); //20 sec wait
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

           UiObject verifyButton = new UiObject(new UiSelector().resourceId("to.talk:id/sign_up_verify_button"));

        if (verifyButton.waitUntilGone(60000)) {

            System.out.println("Auto-verification is successfully done");
            return (true);
        }
        else {

            System.out.println("Auto-verification didn't happen in 60 seconds.");
            return (false);
        }

    }

/*
        public static void waitForAutoVerification() throws UiObjectNotFoundException {

        //make the test wait here for sometime while the app auto reads the verification code

            try {
                Thread.sleep(20000); //20 sec wait
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //check if the the auto verification worked and app id off this screen
            //   UiSelector verifyButtonSelector;
           UiObject verifyButton = new UiObject(new UiSelector().resourceId("to.talk:id/sign_up_verify_button"));
            if (verifyButton.exists()) {

                System.out.println("No verification code received in 20 sec.");

            }
    }

*/


    public static void tapDoneOnProfileUpload() throws UiObjectNotFoundException {

        //tap done post auto verification
        UiObject tapDone =new UiObject((new UiSelector().resourceId("to.talk:id/user_profile_done")));
        tapDone.waitForExists(60000);
        tapDone.clickAndWaitForNewWindow();

        try {
            Thread.sleep(4000); //4 sec wait
            }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void swipeSkipFeatureTour() throws UiObjectNotFoundException {

        try {
            Thread.sleep(2000); //2 sec wait
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        UiObject buttonForNewFeature = new UiObject(new UiSelector().resourceId("to.talk:id/feature_tour_screen_footer"));
        UiObject textForCurrentFeature = new UiObject(new UiSelector().resourceId("to.talk:id/feature_tour_screen_footer"));
        while((!textForCurrentFeature.getText().equals("Start Texting"))) {
            buttonForNewFeature.click();
            buttonForNewFeature = new UiObject(new UiSelector().resourceId("to.talk:id/feature_tour_screen_footer"));
            textForCurrentFeature = new UiObject(new UiSelector().resourceId("to.talk:id/feature_tour_screen_footer"));
        }



        if(textForCurrentFeature.getText().equals("Start Texting")) buttonForNewFeature.click();
        try {
            Thread.sleep(2000); //2 sec wait
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //tabName here can be CHATS/GROUPS/CONTACTS. This method opens the chats/groups/contacts tabs based on parameters passed
    public static void goToTab(String tabName) throws UiObjectNotFoundException {
        // TODO Auto-generated method stub
        try{

            UiObject talktoTab = new UiObject(new UiSelector().resourceId("to.talk:id/title").text(tabName));
            talktoTab.click();
        }
        catch (UiObjectNotFoundException ex) {
            Log.v("TalktoTestUtils","UI object not found: Chats tab");
        }
    }

    //eg: add Saransh Sharma(9871971695), Stephan Hawking(9899651118), Dan Dennett(9920947661) to the group
    public static void addPersonToGrp (String addPerson) throws UiObjectNotFoundException {
        // TODO: this method could take an array of strings instead of a single string and then put all the people in group itrativly
        try {
            UiObject addPersonField = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));
            addPersonField.setText(addPerson);

            UiObject selectPerson = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_tip"));
            selectPerson.click();

            UiObject addPersonDoneButton = new UiObject(new UiSelector().resourceId("to.talk:id/create_group_done_item"));
            addPersonDoneButton.clickAndWaitForNewWindow();
        }
        catch (UiObjectNotFoundException ex){
            Log.v("TalktoTestUtils", "UI object not found: Chats tab");
        }
    }

    public static int randomGenerator() {

        //todo: fix this random number gen and use it to set unique group names, send unique messages

        int randomNumber = (int) (Math.random() * 10000);
        return randomNumber;

    }

    public static void newMessageNandu(String contactNameNumber, String msgPayload) throws UiObjectNotFoundException {

        //tap on new message button
        UiObject newMessageButton = new UiObject(new UiSelector().resourceId("to.talk:id/menu_home_activity_start_conversation"));
        newMessageButton.clickAndWaitForNewWindow();

        //search for contact in string contactName
        UiObject contactSearch = new UiObject(new UiSelector().resourceId("to.talk:id/createGroupMultiChipTextView"));
        contactSearch.setText(contactNameNumber);

        //enter the message
        UiObject messageBox = new UiObject(new UiSelector().resourceId("to.talk:id/message_field"));
        messageBox.setText(msgPayload);

        UiObject sendButton = new UiObject(new UiSelector().resourceId("to.talk:id/send"));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("2 sec thread sleep in newMessageNandu method failed");
        }

        sendButton.click();


    }

    public static void tapLearnMore() throws UiObjectNotFoundException {

        UiObject learnButton = new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_blurb_learn_more_button"));
        learnButton.clickAndWaitForNewWindow();

    }


    public static void setDefaultFromBlurb() throws UiObjectNotFoundException {

        //Tap the 'Set as Default SMS app' from blurb
        UiObject defaultBlurbButton = new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_blurb_set_default_button"));
        defaultBlurbButton.click();

        // KitKat devices ask for conformation. The following taps 'yes', if asked.
        UiObject defaultConfirmPromptTitle = new UiObject(new UiSelector().resourceId("android:id/alertTitle"));

        if (defaultConfirmPromptTitle.exists()) {

            UiObject yesButton = new UiObject(new UiSelector().resourceId("android:id/button1"));
            yesButton.click();
        }
    }

    public static void tapLearnSetLater() throws UiObjectNotFoundException {

        //tap 'learn more' button on the default SMS blurb
        UiObject learnButton = new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_blurb_learn_more_button"));
        learnButton.clickAndWaitForNewWindow();

        //tap set default SMS later button
        UiObject defaultSMSLater= new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_app_activity_later"));
        defaultSMSLater.clickAndWaitForNewWindow();

    }

    //set talk.to as default SMS app from the screen that opens tapping on 'learn more'
    public static void tapLearnSetDefault() throws UiObjectNotFoundException {

        UiObject learnButton = new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_blurb_learn_more_button"));
        learnButton.clickAndWaitForNewWindow();

        UiObject setDefault = new UiObject(new UiSelector().resourceId("to.talk:id/default_sms_app_activity_agree_button"));
        setDefault.click();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // KitKat devices ask for conformation. The following taps 'yes', if asked.
        UiObject defaultConfirmPromptTitle = new UiObject(new UiSelector().resourceId("android:id/alertTitle"));

        //if (defaultConfirmPromptTitle.getText().equals("Change SMS app?")) {
        if (defaultConfirmPromptTitle.exists()){

            UiObject yesButton = new UiObject(new UiSelector().resourceId("android:id/button1"));
            yesButton.click();
        }
    }

    //this taps continue as X in case of Single Step Sign in
    public static void tapContinueAsX() throws UiObjectNotFoundException {

        UiObject welcomeTitle= new UiObject(new UiSelector().resourceId("to.talk:id/welcome_back_main_heading"));
        assert welcomeTitle.exists();

        //tap on continue as X. The resource ID may suggest otherwise but this is the continue as X button
        UiObject continueAsX = new UiObject(new UiSelector().resourceId("to.talk:id/not_you_continue"));
        continueAsX.clickAndWaitForNewWindow();

    }

    //this taps 'not X' in case of Single Step Sign in
    public static void tapNotX() throws UiObjectNotFoundException {

        UiObject notButton = new UiObject(new UiSelector().resourceId("to.talk:id/not_you_footer_button"));
        notButton.clickAndWaitForNewWindow();

    }

    public static void sendMessage(String message) throws UiObjectNotFoundException {

        UiObject sendMessageBox = new UiObject(new UiSelector().resourceId("to.talk:id/message_field"));
        sendMessageBox.setText(message);

        UiObject sendButton = new UiObject(new UiSelector().resourceId("to.talk:id/send_sms"));
        if (sendButton.isEnabled()){

            sendButton.click();
        } else System.out.println("Looks like the send button is disabled"); //todo: make test fail here


    }


    public static void usePassword(String password) throws UiObjectNotFoundException, InterruptedException {

        UiObject passwordTextbox = new UiObject(new UiSelector().resourceId("to.talk:id/sign_up_welcome_back_password"));
        passwordTextbox.setText(password);

        UiObject signInButton = new UiObject(new UiSelector().resourceId("to.talk:id/sign_up_welcome_back_sign_in"));
        /*if (!signInButton.isEnabled()) {

        }
        signInButton.click();*/

        if(TalktoTestUtils.isButtonEnabled(signInButton, 20)) {

            signInButton.click();
        }
        else System.out.println("Test failed as the 'Sign in' button was disabled");

        //wait for a bit and check if you are on the same page
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        UiObject welcomeTitle1 = new UiObject(new UiSelector().resourceId("to.talk:id/welcome_back_main_heading"));
        if (welcomeTitle1.exists()){
            System.out.println("The password provided is incorrect");

    //todo handle the incorrect password case

        }
    }



    public static void getNewPassword() throws UiObjectNotFoundException {

        UiObject dontHavePasswordButton = new UiObject(new UiSelector().resourceId("to.talk:id/sign_up_welcome_back_dnt_have_password"));
        dontHavePasswordButton.clickAndWaitForNewWindow();
    }

    public static void talktoSignIn(String phoneNumber, String password) throws UiObjectNotFoundException, InterruptedException {

        //todo: if this returns 'incorrect password', write and call later a password recovery method

        TalktoTestUtils.talktoSignUp("F_name","L_name",phoneNumber);

        //todo check if I am on welcome back screen

        UiObject welcomeTitle = new UiObject(new UiSelector().resourceId("to.talk:id/welcome_back_main_heading"));
        assert(welcomeTitle.exists());

        TalktoTestUtils.usePassword(password);

        //if (!welcomeTitle.exists()){
        //System.out.println("Script seems to be lost. Its supposed to be on welcome back screen, but it isn't");
        //}



    }


    public static void searchFor(String personOrNumber) throws UiObjectNotFoundException {
        //todo: use Dan Dannett Nandu till file thing isn't fixed

        String firstLetter = Character.toString(personOrNumber.charAt(0));

        UiObject searchButton = new UiObject(new UiSelector().resourceId("to.talk:id/menu_home_activity_search_contact"));
        searchButton.clickAndWaitForNewWindow();

        UiObject searchTextBox = new UiObject(new UiSelector().resourceId("to.talk:id/search_box"));
        searchTextBox.setText(firstLetter);

        UiScrollable scrollableResults = new UiScrollable(new UiSelector().resourceId("to.talk:id/list").scrollable(true));

        //UiObject titleMatch = scrollableResults.getChildByDescription(new UiSelector().resourceId("to.talk:id/title"),personOrNumber ,true);
        //UiObject titleMatch = scrollableResults.getChildByDescription(new UiSelector().resourceId("to.talk:id/title"), personOrNumber);
        UiObject titleMatch = scrollableResults.getChildByText(new UiSelector().resourceId("to.talk:id/title"), personOrNumber);
        titleMatch.click();
        /*
        if (titleMatch.exists()){
            titleMatch.clickAndWaitForNewWindow();
            System.out.println(titleMatch.getText());
        } else
            System.out.println("Couldn't find " +personOrNumber +" in roster.");
        */
    }


    public static boolean checkAppHomeScreenExistsInFg(int minutes) throws UiObjectNotFoundException {

        try {
            Thread.sleep(minutes * 60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        goToTab("CONTACTS");

        UiObject chatTab = new UiObject(new UiSelector().resourceId("to.talk:id/title").text("CONTACTS"));
        if (chatTab.exists()) {

            System.out.println("The app still exists in fg after " +minutes +" minutes");
            return true;

        }
        else {
        System.out.println("The app does Not exists in fg after " +minutes +" minutes." + "Possible crash");
        return false;
            }

    }

    public String readTextFile(String fileName) {

        String returnValue = "";
        FileReader file = null;

        try {
            file = new FileReader(fileName);
            BufferedReader reader = new BufferedReader(file);
            String line = "";
            while ((line = reader.readLine()) != null) {
                returnValue += line + "\n";
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (IOException e) {
                    // Ignore issues during closing
                }
            }
        }
        return returnValue;
    }

    public void writeTextFile(String fileName, String s) {
        FileWriter output = null;
        try {
            output = new FileWriter(fileName);
            BufferedWriter writer = new BufferedWriter(output);
            writer.write(s);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    // Ignore issues during closing
                }
            }
        }

    }

/*
    public boolean isButtonEnabled(UiObject button, int maxWaitInSeconds) throws UiObjectNotFoundException, InterruptedException {

        int waitCounter = 0;
        while (!button.isEnabled()) { //while button is disabled
            wait(1000); //wait for 1 sec
            waitCounter++;

            if (maxWaitInSeconds/waitCounter == 0){
                //todo: test logger/report should say that the timeout is reached and button is still disabled. Either the button is broken or there are network issues
                System.out.println("max wait time reached " +waitCounter);
                return false;
                break;
            }
        }


        if (button.isEnabled())

            return true;

    }

*/


    public static boolean isButtonEnabled(UiObject button, int timeoutInSec) throws UiObjectNotFoundException, InterruptedException {

        for (int i =1; !button.isEnabled() && i <= timeoutInSec; ) { //true and true. this will loop till either the button is enabled or time out is reached

            Thread.sleep(1000); //sleep for a sec
            System.out.println("loop run count " +i);
            i++;
            if (i == timeoutInSec) {
                System.out.println("Timeout reached, button is still disabled");
                return false;
            }
        }
        return true;
    }


/*

    public static boolean isPackageInstalled(String packageName) {
        try {
            //Context application;
            application.getPackageManager().getPackageInfo(packageName, 0);
        } catch (NameNotFoundException e) {
            return false;
        }
        return true;
    }


*/


}




/*
    public static void findAndOpenApp() throws UiObjectNotFoundException  {
        try{

            UiObject allAppsButton = new UiObject(new UiSelector().description("Apps"));

            allAppsButton.clickAndWaitForNewWindow();

            UiObject appsTab = new UiObject(new UiSelector().text("Apps"));

            appsTab.click();
            UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));

            appViews.setAsHorizontalList(); // Set the swiping mode to horizontal (the default is vertical)

            // Create a UiSelector to find the Talk.to app and simulate
            // a user click to launch the app.
            UiObject talktoApp = appViews.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()),"Talk.to");
            talktoApp.clickAndWaitForNewWindow();

            // Validate that the package name is the expected one
            UiObject ttValidation = new UiObject(new UiSelector().packageName("to.talk"));
            assertTrue("Unable to detect Talk.TO",ttValidation.exists());
        }
        catch (UiObjectNotFoundException ex){
            Log.v("clearAppdata" ,"UI object not found exception in find and open app");
        }
    }
*/




